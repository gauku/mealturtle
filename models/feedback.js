var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var feedbackSchema = new Schema({
    quality: { type: Number },
    quantity: { type: Number },
    taste: { type: Number },
    feedback: { type: String },
    feedbackDate: {type: String}
});

var Feedback = mongoose.model('Feedback', feedbackSchema);
module.exports = Feedback;