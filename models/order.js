var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.connect('mongodb://userdbadmin:userdbpwdadmin@localhost:27017/userdb');

var orderSchema = new Schema({
    totalamount: { type: Number },
    Name: {type: String},
    mobile: {type: Number},
    slot: {type: String},
    locationid: {type: String},
    address: {type: String},
	updated: { type: Date, default: Date.now },
	Curry_1: {type: String},
    Curry_2: {type: String},
    roti: {type: Number},
    rice: {type: String},
    dal: {type: String},
    salad: {type: String},
    orderDate: {type: String}
});

// module.exports.authenticate = function(cb) {
//   cb;
// };
var Order = mongoose.model('Order', orderSchema);

module.exports = Order;
