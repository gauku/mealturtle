var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var locationSchema = new Schema({
    email: { type: String },
    companyname: { type: String },
    requestDate: {type: String}
});

var Location = mongoose.model('Location', locationSchema);

module.exports = Location;