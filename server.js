//1. Make Hinjewadi Phase 1 the default selection as long as it's the only option available
//1. Add vibration effect or something that shows the user that the price has changed
//2. Add notifications upon placing of order, feedback and location request (http://www.gianlucaguarini.com/blog/nodejs-and-a-simple-push-notification-server/)
//2. Add an About Us Page
//3. Add weekly/monthly menus
//4. Paytm integration
//5. Bill mailing
//6. Edit Feedback
//7. Try authentication
'use strict';
    
//var companies = {"Pune" : ["Siemens Industry Software", "3DPLM", "Geometric"]};
const Hapi = require('hapi');
const Path = require('path');

let uuid = 1;       // Use seq instead of proper unique identifiers for demo only

// var options = {
//     path: 'public',
//     url: '/menu/{locationid?}',
//     listing: true,
//     index: true,
//     cache: 3600000
// };

// const homeget = function (request, reply) {
//     if(request.auth.isAuthenticated)
//     {
//         console.log('going to menu page');
//         reply.redirect('/menu/' + request.auth.credentials.locationID);
//     }
//     else
//     {
//       reply.file('./public/index.html');
//     }
// };

const homepost = function (request, reply) {
    reply.redirect('/menu');
};

// const menuget = function(request, reply) {
//     if(!request.params.locationid && !request.auth.isAuthenticated)
//     {
//         console.log('going back to home');
//         reply.redirect('/');
//     }
//     var locationid;
//     if(!request.params.locationid && request.auth.isAuthenticated)
//     {
//         locationid = request.auth.credentials.locationID;
//     }
//     else
//     {
//         locationid = encodeURIComponent(request.params.locationid);
//     }
//     var Order = require('./models/order');
//     let account = null;

//     var newOrder = new Order({
//         locationID: locationid,
//     });

//     newOrder.save(function(err) {
//         if (err)
//             throw err;
//     })
//     account = newOrder;
//     const sid = String(++uuid);
//     request.server.app.cache.set(sid, { account: account }, 0, (err) => {

//         if (err) {
//             reply(err);
//         }

//         request.cookieAuth.set({ sid: sid });
//     });
//         reply.file('./public/menu.html');
// };

const menupost = function (request, reply) {     //menu
    let account = null;
    var newOrder;
    var Order = require('./models/order');

    var newOrder = new Order({
        locationid: request.payload.locationid,
        totalamount: request.payload.totalamount,
        Name: request.payload.Name,
        mobile: request.payload.mobile,
        slot: request.payload.slot,
        address: request.payload.address
    });

    if(request.payload.Curry_1)
        newOrder.Curry_1 = request.payload.Curry_1;
    if(request.payload.Curry_2)
        newOrder.Curry_2 = request.payload.Curry_2;
    if(request.payload.roti)
        newOrder.roti = request.payload.roti;
    if(request.payload.rice)
        newOrder.rice = request.payload.rice;
    if(request.payload.dal)
        newOrder.dal = request.payload.dal;
    if(request.payload.salad)
        newOrder.salad = request.payload.salad;

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd='0'+dd
    } 

    if(mm<10) {
        mm='0'+mm
    } 

    today = dd+'/'+mm+'/'+yyyy;
    newOrder.orderDate = today;

    var finalPrice = 0;
    if(request.payload.Curry_1 && request.payload.Curry_2)
        finalPrice = finalPrice + 35;
    else if(request.payload.Curry_1 || request.payload.Curry_2)
        finalPrice = finalPrice + 20;
    if(request.payload.roti)
        finalPrice = finalPrice + 5*(request.payload.roti);
    if(request.payload.rice)
        finalPrice = finalPrice + 20;
    if(request.payload.dal)
        finalPrice = finalPrice + 20;

    if(request.payload.totalamount != finalPrice)
        newOrder.totalamount = finalPrice;

    newOrder.save(function(err) {
        if (err)
            throw err;
    });
    account = newOrder;
    const sid = String(++uuid);
    request.server.app.cache.set(sid, { account: account }, 0, (err) => {

        if (err) {
            reply(err);
        }

        request.cookieAuth.set({ sid: sid });
    });

    return ;
};

const locationnotlisted = function(request, reply) {
    var Location = require('./models/location');
    var newlocation = new Location({
        email: request.payload.email,
        companyname: request.payload.companyname
    });    
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
        var time = today.getTime();
console.log(today);
    if(dd<10) {
        dd='0'+dd
    } 

    if(mm<10) {
        mm='0'+mm
    } 

    today = dd+'/'+mm+'/'+yyyy;
    newlocation.requestDate = today;
    newlocation.save(function(err) {
        if (err)
            throw err;
    });
    reply.redirect('/');
};

const feedback = function(request, reply) {
    var Feedback = require('./models/feedback');
    var newfeedback = new Feedback({
        quality: request.payload.quality,
        quantity: request.payload.quantity,
        taste: request.payload.taste,
        feedback: request.payload.feedback
    });    
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd='0'+dd
    } 

    if(mm<10) {
        mm='0'+mm
    } 

    today = dd+'/'+mm+'/'+yyyy;
    newfeedback.feedbackDate = today;
    newfeedback.save(function(err) {
        if (err)
            throw err;
    });
    console.log(newfeedback);
    reply.redirect('/booking.html');
};

const someesotericrouteforordersthatcantbeunderstoodorhitbyanyhumanbeing = function(request, reply) {
    var Order = require('./models/order');
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd='0'+dd
    } 

    if(mm<10) {
        mm='0'+mm
    } 

    today = dd+'/'+mm+'/'+yyyy;
    Order.find({orderDate: today}, function(err, user) {
        reply(user);
    }
    );
};

const someesotericrouteforfeedbacksthatcantbeunderstoodorhitbyanyhumanbeing = function(request, reply) {
    var Feedback = require('./models/feedback');
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd='0'+dd
    } 

    if(mm<10) {
        mm='0'+mm
    } 

    today = dd+'/'+mm+'/'+yyyy;
    Feedback.find({feedbackDate: today}, function(err, feedback) {
        reply(feedback);
    }
    );
};

const someesotericroutefornewserverequeststhatcantbeunderstoodorhitbyanyhumanbeing = function(request, reply) {
    var Location = require('./models/location');
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd='0'+dd
    } 

    if(mm<10) {
        mm='0'+mm
    } 

    today = dd+'/'+mm+'/'+yyyy;
    Location.find({requestDate: today}, function(err, locationrequest) {
        reply(locationrequest);
    }
    );
};

const server = new Hapi.Server();

server.connection({
        host: 'ec2-52-40-182-104.us-west-2.compute.amazonaws.com',
        port: 8080
});


server.register([require('hapi-auth-cookie'), require('inert'), require('blipp')], (err) => {
    if (err) {
        throw err;
    }

    const cache = server.cache({ segment: 'sessions', expiresIn: 3 * 24 * 60 * 60 * 1000 });
    server.app.cache = cache;

    server.auth.strategy('session', 'cookie', true, {
        ttl: 15 * 24 * 60 * 60 * 1000,
        password: 'somesuperrandompasswordthatcantbefuckingthoughtofbyanyfuckingbotorchutiyalundhuman',
        cookie: 'sid-example',
        redirectTo: '/',
        isSecure: false,
        validateFunc: function (request, session, callback) {

            cache.get(session.sid, (err, cached) => {

                if (err) {
                    return callback(err, false);
                }

                if (!cached) {
                    return callback(null, false);
                }

                return callback(null, true, cached.account);
            });
        }
    });

    server.route([
        { method: 'GET', path: '/{file*}',
        config:{
         handler: {
            directory: {
                path: 'public',
                listing: true,
                index: true
            }
        }, auth: { mode: 'try' }, plugins: { 'hapi-auth-cookie': { redirectTo: false } } } } ,
       
        { method: 'POST', path: '/', config: { handler: homepost, auth: { mode: 'try' }, plugins: { 'hapi-auth-cookie': { redirectTo: false } } } },
       
        // { method: 'GET', path: '/menu/{locationid*}', 
        // config: { 
        //     handler: {
        //     directory: {
        //         path: 'pubic',
        //         listing: true,
        //         index: true
        //     }
        // }, auth: { mode: 'try' }, plugins: { 'hapi-auth-cookie': { redirectTo: false } } } },
      
        { method: 'POST', path: '/menu', config: { handler: menupost, auth: { mode: 'try' }, plugins: { 'hapi-auth-cookie': { redirectTo: false } } } },
      
   //     { method: 'GET', path: '/checkout', config: { handler: checkout } },

        { method: 'GET',
        path: '/someesotericrouteforordersthatcantbeunderstoodorhitbyanyhumanbeing', 
        config: { handler: someesotericrouteforordersthatcantbeunderstoodorhitbyanyhumanbeing, 
            auth: { mode: 'try' }, 
            plugins: { 'hapi-auth-cookie': { redirectTo: false } } } },

        { method: 'GET', 
        path: '/someesotericroutefornewserverequeststhatcantbeunderstoodorhitbyanyhumanbeing', 
        config: { handler: someesotericroutefornewserverequeststhatcantbeunderstoodorhitbyanyhumanbeing, 
            auth: { mode: 'try' }, 
            plugins: { 'hapi-auth-cookie': { redirectTo: false } } } },
        { method: 'GET', 
        path: '/someesotericrouteforfeedbacksthatcantbeunderstoodorhitbyanyhumanbeing', 
        config: { handler: someesotericrouteforfeedbacksthatcantbeunderstoodorhitbyanyhumanbeing, 
            auth: { mode: 'try' }, 
            plugins: { 'hapi-auth-cookie': { redirectTo: false } } } },

        { method: 'POST', path: '/locationnotlisted', config: { handler: locationnotlisted, auth: { mode: 'try' }, plugins: { 'hapi-auth-cookie': { redirectTo: false } } } },

        { method: 'POST', path: '/feedback', config: { handler: feedback, auth: { mode: 'try' }, plugins: { 'hapi-auth-cookie': { redirectTo: false } } } }
    ]);

    server.start(() => {

        console.log('Server ready');
    });
});
